from random import randint
# prompt name
name = input('Hi! What is your name? ')

# loops up to 5 times
for num in range(5) :
    # only guess years between 1924 and 2004 including those years.
    year = randint(1924, 2004)
    month = randint(1, 12)
    # guess birthday month n year also a prompt yes or no
    print('Guess ', num + 1 ,' : ', name ,' were you born in ', month ,' / ', year ,' ?')
    response = input('yes or no? ').lower()

# if guess is correct , print 'I knew it!' and break
    if response == 'yes':
        print('I knew it!')
        break
    else:
        # if incorrect but within 4 tries, print 'Drat! Lemme try again!'
        if num < 4:
            print('Drat! Lemme try again!')
        else:
            # tries >4, print 'I have other things to do. Good bye.'
            print('I have other things to do. Good bye.')
            
        
    








